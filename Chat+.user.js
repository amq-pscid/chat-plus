// ==UserScript==
// @name         Chat+
// @version      1.0.0
// @description  Chat+
// @author       PsCid
// @match        https://animemusicquiz.com/*
// @grant        none
// @updateURL    https://gitlab.com/amq-pscid/chat-plus/-/raw/master/Chat+.user.js
// ==/UserScript==

const boldRegex = /\*\*(.*?)\*\*/g;
let cronoCommand = "/crono";
let cronoLimit = 360;

// Main
async function checkMessage(msgId, args) {
    await delay(50);
    var msgElement = document.getElementById(msgId);
    var playerImgMessage = msgElement.getElementsByClassName("gcMessage")[0].innerText;
    var playerMessage = msgElement.getElementsByClassName("gcMessage")[0].innerHTML;
    if ((playerImgMessage.startsWith("http://") || playerImgMessage.startsWith("https://"))
        && (playerImgMessage.endsWith(".png") || playerImgMessage.endsWith(".jpg") || playerImgMessage.endsWith(".jpeg") || playerImgMessage.endsWith(".gif") || playerImgMessage.endsWith(".webp"))) {
        var img = new Image();
        img.onload = function () {
            img.style.width = '100%';
            img.style.height = 'auto';
            img.style.overflow = 'visible';
            msgElement.appendChild(img);
            document.getElementById("gcMessageContainer").scrollTo(0,$("#gcMessageContainer").prop("scrollHeight"));
        }
        img.src = playerImgMessage;
    } else if (playerMessage.includes("**")) {
        var msgArray = extractAllText(playerMessage, boldRegex);
        for (var i = 0; i < msgArray.length; ++i) {
            var boldMsgPart = msgArray[i].bold();
            playerMessage = playerMessage.replace("**" + msgArray[i] + "**", boldMsgPart);
        }
        msgElement.getElementsByClassName("gcMessage")[0].innerHTML = playerMessage;
    }
    if (args.length >= 2 && args[0].startsWith("/") && colorList.includes(args[0].substring(1, args[0].length))) {
        var msgToColor = msgElement.getElementsByClassName("gcMessage")[0];
        msgToColor.innerHTML = playerMessage.replace(args[0] + " ", "");
        msgToColor.style.color = args[0].substring(1, args[0].length);
    }
}

function extractAllText(str, re){
    const result = [];
    let current;
    while (current = re.exec(str)) {
        result.push(current.pop());
    }
    return result.length > 0
        ? result
    : [str];
}

async function cronoMsg(msgId, seconds) {
    if (seconds <= cronoLimit) {
        await delay(50);
        var msgElement = document.getElementById(msgId);
        var currentSeconds = seconds;
        for (var i = 0; i < seconds; ++i) {
            await delay(1000);
            currentSeconds--;
            msgElement.getElementsByClassName("gcMessage")[0].innerHTML = currentSeconds;
        }
    }
}

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function isHidden(el) {
    return (el.offsetParent === null)
}

function hasId(element){
    return typeof element.id != 'undefined';
}

// Setup
let loadInterval = setInterval(() => {
    if (document.getElementById("loadingScreen").classList.contains("hidden")) {
        setup();
        clearInterval(loadInterval);
    }
}, 500);

function setup() {
    let commandListener = new Listener("game chat update", (payload) => {
        payload.messages.forEach(message => {
            var msgId = "gcPlayerMessage-" + message.messageId;
            let args = message.message.split(/\s+/);
            if (message.message.startsWith(cronoCommand)) {
                if (args.length == 2 && !isNaN(parseInt(args[1]))) {
                    cronoMsg(msgId, parseInt(args[1]));
                }
            } else {
                checkMessage(msgId, args);
            }
        });
    });

    commandListener.bindListener();
}

var colorList = ["aliceblue", "lightsalmon", "antiquewhite", "lightseagreen", "aqua", "lightskyblue", "aquamarine", "lightslategray", "azure", "lightsteelblue", "beige", "lightyellow", "bisque", "lime", "black", "limegreen", "blanchedalmond", "linen", "blue", "magenta", "blueviolet", "maroon", "brown", "mediumaquamarine", "burlywood", "mediumblue", "cadetblue", "mediumorchid", "chartreuse", "mediumpurple", "chocolate", "mediumseagreen", "coral", "mediumslateblue", "cornflowerblue", "mediumspringgreen", "cornsilk", "mediumturquoise", "crimson", "mediumvioletred", "cyan", "midnightblue", "darkblue", "mintcream", "darkcyan", "mistyrose", "darkgoldenrod", "moccasin", "darkgray", "navajowhite", "darkgreen", "navy", "darkkhaki", "oldlace", "darkmagenta", "olive", "darkolivegreen", "olivedrab", "darkorange", "orange", "darkorchid", "orangered", "darkred", "orchid", "darksalmon", "palegoldenrod", "darkseagreen", "palegreen", "darkslateblue", "paleturquoise", "darkslategray", "palevioletred", "darkturquoise", "papayawhip", "darkviolet", "peachpuff", "deeppink", "peru", "deepskyblue", "pink", "dimgray", "plum", "dodgerblue", "powderblue", "firebrick", "purple", "floralwhite", "red", "forestgreen", "rosybrown", "fuchsia", "royalblue", "gainsboro", "saddlebrown", "ghostwhite", "salmon", "gold", "sandybrown", "goldenrod", "seagreen", "gray", "seashell", "green", "sienna", "greenyellow", "silver", "honeydew", "skyblue", "hotpink", "slateblue", "indianred", "slategray", "indigo", "snow", "ivory", "springgreen", "khaki", "steelblue", "lavender", "tan", "lavenderblush", "teal", "lawngreen", "thistle", "lemonchiffon", "tomato", "lightblue", "turquoise", "lightcoral", "violet", "lightcyan", "wheat", "lightgoldenrodyellow", "white", "lightgreen", "whitesmoke", "lightgrey", "yellow", "lightpink", "yellowgreen"]